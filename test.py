import pytest
import urllib.request as urR
import re

def findText(text): #function returns true if text is found in the given website
    html_content = urR.urlopen('https://the-internet.herokuapp.com/context_menu').read().decode('utf-8')
    matches = re.findall(text, html_content);
    if len(matches) == 0:
        return 0
    else:
        return 1

def test1():
    assert findText("Right-click in the box below to see one called 'the-internet") == 1

def test2():
    assert findText("Alibaba") == 1

